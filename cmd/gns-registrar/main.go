// This file is part of gnunet-gns-registrar, a GNS registrar service.
// Copyright (C) 2022 Martin Schanzenbach
//
// gnunet-gns-registrar is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// gnunet-gns-registrar is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package main

import (
	"flag"
	"log"
	"net/http"

	gnsregistrar "gnunet.org/gnunet-gns-registrar/pkg/rest"
)

var t gnsregistrar.Registrar

func handleRequests(r *gnsregistrar.Registrar) {
	log.Fatal(http.ListenAndServe(r.Cfg.Section("gns-registrar").Key("bind_to").MustString("localhost:11000"), r.Router))
}

func main() {
	var cfgFlag = flag.String("c", "", "Configuration file to use")

	flag.Parse()
	cfgfile := "gns-registrar.conf"
	if len(*cfgFlag) != 0 {
		cfgfile = *cfgFlag
	}
	t := gnsregistrar.Registrar{}
	t.Initialize(cfgfile)
	handleRequests(&t)
}
