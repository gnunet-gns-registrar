module gnunet.org/gnunet-gns-registrar

go 1.18

require (
	github.com/google/uuid v1.4.0
	github.com/gorilla/mux v1.8.1
	github.com/schanzen/taler-go v1.0.6
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	gopkg.in/ini.v1 v1.67.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
